FROM lukemathwalker/cargo-chef:latest-rust-1.74.0 AS chef
WORKDIR /app

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef AS builder
COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --recipe-path recipe.json

COPY . .
RUN cargo build

FROM rust:1.74-slim AS server
COPY --from=builder /app/target/debug/server /usr/local/bin
ENTRYPOINT ["/usr/local/bin/server"]