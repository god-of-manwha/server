## Project Setup

ENV file for non-docker usage. In order to run database integration tests, the sqlx test crate requires the `DATABASE_URL` env variable configured. Otherwise, the other postgres env variables can be optionally configured in docker compose exclusively. They are only necessary for a local `cargo run`.

```
POSTGRES_PASSWORD=testpass
POSTGRES_USER=testuser
POSTGRES_DB=godofmanwha
POSTGRES_HOST=localhost:5432
DATABASE_URL=postgresql://testuser:testpass@localhost:5432/godofmanwha
```

## Migrations

This relies on sqlx-cli in order to run the migrations. After the database is up and running, and the `DATABASE_URL` env is configured, running `sqlx migrate run` will set up your database. Installing the sqlx-cli is just `cargo install sqlx-cli`.
