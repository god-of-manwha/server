-- Add up migration script here
CREATE TABLE IF NOT EXISTS posts (
    id UUID PRIMARY KEY,
    title VARCHAR NOT NULL,
    body TEXT,
    slug VARCHAR UNIQUE NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ NOT NULL
);

CREATE INDEX slug_lookup ON posts (LOWER(slug));