use axum::http;

pub async fn get_health() -> http::StatusCode {
    http::StatusCode::OK
}
