#[cfg(test)]
mod tests {
    use crate::controllers::health;
    use axum::http;

    #[tokio::test]
    async fn get_health_test() {
        let response = health::get_health().await;
        assert_eq!(response, http::StatusCode::OK)
    }
}
