use crate::models::post;
use crate::services::posts::{self, PostError};
use axum::{
    extract::{Path, State},
    http::{self, header, HeaderMap},
    Json,
};
use sqlx::PgPool;

pub async fn create_post(
    State(pool): State<PgPool>,
    Json(payload): Json<post::CreatePost>,
) -> Result<(http::StatusCode, HeaderMap), http::StatusCode> {
    let post = post::Post::new(payload.title, payload.body, payload.slug);
    let result = posts::create_post(&pool, &post).await;
    let mut headers = HeaderMap::new();
    headers.insert(
        header::LOCATION,
        format!("/posts/{}", &post.id).parse().unwrap(),
    );

    match result {
        Ok(_) => Ok((http::StatusCode::CREATED, headers)),
        Err(err) => match err {
            PostError::DuplicateError => Err(http::StatusCode::CONFLICT),
            _ => Err(http::StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}

pub async fn get_posts(
    State(pool): State<PgPool>,
) -> Result<Json<Vec<post::Post>>, http::StatusCode> {
    let result = posts::get_posts(&pool).await;

    match result {
        Ok(posts) => Ok(Json(posts)),
        Err(_) => Err(http::StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_post(
    State(pool): State<PgPool>,
    Path(id): Path<String>,
) -> Result<Json<post::Post>, http::StatusCode> {
    let Ok(uuid) = uuid::Uuid::parse_str(&id) else {
        let result = posts::get_post_by_slug(&pool, &id).await;

        return match result {
            Ok(post) => Ok(Json(post)),
            Err(_) => Err(http::StatusCode::NOT_FOUND),
        };
    };

    let result = posts::get_post_by_uuid(&pool, &uuid).await;

    match result {
        Ok(post) => Ok(Json(post)),
        Err(_) => Err(http::StatusCode::NOT_FOUND),
    }
}

pub async fn update_post(
    State(pool): State<PgPool>,
    Path(id): Path<uuid::Uuid>,
    Json(payload): Json<post::CreatePost>,
) -> Result<http::StatusCode, http::StatusCode> {
    let result = posts::update_post(&pool, &id, &payload).await;

    match result {
        Ok(_) => Ok(http::StatusCode::NO_CONTENT),
        Err(err) => match err {
            PostError::NotFoundError => Err(http::StatusCode::NOT_FOUND),
            _ => Err(http::StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}

pub async fn delete_post(
    State(pool): State<PgPool>,
    Path(id): Path<uuid::Uuid>,
) -> Result<http::StatusCode, http::StatusCode> {
    let result = posts::delete_post(&pool, &id).await;

    match result {
        Ok(_) => Ok(http::StatusCode::NO_CONTENT),
        Err(err) => match err {
            PostError::NotFoundError => Err(http::StatusCode::NOT_FOUND),
            _ => Err(http::StatusCode::INTERNAL_SERVER_ERROR),
        },
    }
}
