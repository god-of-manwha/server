#[cfg(test)]
mod tests {
    use crate::controllers::posts;
    use crate::models::post;
    use axum::http;
    use regex::Regex;
    use sqlx::PgPool;

    #[sqlx::test(fixtures("posts"))]
    async fn test_get_posts(pool: PgPool) {
        let response = posts::get_posts(axum::extract::State(pool))
            .await
            .expect("Failed to get posts.")
            .0;

        assert!(response.len() > 0);
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_get_post(pool: PgPool) {
        let posts = posts::get_posts(axum::extract::State(pool.clone()))
            .await
            .expect("Failed to get posts.")
            .0;

        for post in posts {
            let response = posts::get_post(
                axum::extract::State(pool.clone()),
                axum::extract::Path(post.id.clone().to_string()),
            )
            .await
            .expect("Failed to get post.")
            .0;

            assert_eq!(response, post);
        }
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_get_post_not_found(pool: PgPool) {
        let response = posts::get_post(
            axum::extract::State(pool.clone()),
            axum::extract::Path(uuid::Uuid::new_v4().to_string()),
        )
        .await;

        match response {
            Ok(_) => assert!(false),
            Err(err) => assert_eq!(err, http::StatusCode::NOT_FOUND),
        }
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_create_posts(pool: PgPool) {
        let get_posts_res = posts::get_posts(axum::extract::State(pool.clone()))
            .await
            .expect("Failed to get posts.")
            .0;

        assert_eq!(get_posts_res.len(), 3);

        let create_post_json = post::CreatePost {
            title: String::from("Some Title"),
            body: String::from("Some Body"),
            slug: String::from("some-slug"),
        };

        let create_post_res = posts::create_post(
            axum::extract::State(pool.clone()),
            axum::Json(create_post_json.clone()),
        )
        .await
        .expect("Failed to create post.");

        let status_code = create_post_res.0;
        let header_map = create_post_res.1;

        assert_eq!(status_code, http::StatusCode::CREATED);

        let location = header_map.get(http::header::LOCATION).unwrap();
        let location = location.to_str().unwrap();
        let location_regex = Regex::new(r"/posts/(?<uuid>[\w-]+)").unwrap();
        let regex = location_regex.captures(location).unwrap();
        let uuid = &regex["uuid"];

        let get_posts_res = posts::get_posts(axum::extract::State(pool.clone()))
            .await
            .expect("Failed to get posts.")
            .0;

        assert_eq!(get_posts_res.len(), 4);

        let response = posts::get_post(
            axum::extract::State(pool.clone()),
            axum::extract::Path(uuid.to_string()),
        )
        .await
        .expect("Failed to get post.")
        .0;

        assert_eq!(response.title, create_post_json.title);
        assert_eq!(response.body, create_post_json.body);
        assert_eq!(response.slug, create_post_json.slug);
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_create_duplicate_slug(pool: PgPool) {
        let get_posts_res = posts::get_posts(axum::extract::State(pool.clone()))
            .await
            .expect("Failed to get posts.")
            .0;

        assert_eq!(get_posts_res.len(), 3);

        let create_post_json = post::CreatePost {
            title: String::from("Some Title"),
            body: String::from("Some Body"),
            slug: String::from("random-slug-1"),
        };

        let Err(status_code) = posts::create_post(
            axum::extract::State(pool.clone()),
            axum::Json(create_post_json.clone()),
        )
        .await
        else {
            return assert!(false);
        };

        assert_eq!(status_code, http::StatusCode::CONFLICT);

        let get_posts_res = posts::get_posts(axum::extract::State(pool.clone()))
            .await
            .expect("Failed to get posts.")
            .0;

        assert_eq!(get_posts_res.len(), 3);
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_update_post(pool: PgPool) {
        let get_posts_res = posts::get_posts(axum::extract::State(pool.clone()))
            .await
            .expect("Failed to get posts.")
            .0;

        let post = get_posts_res.first().unwrap();
        let create_post = post::CreatePost {
            title: post.title.clone(),
            body: post.body.clone(),
            slug: post.slug.clone(),
        };

        let response = posts::update_post(
            axum::extract::State(pool.clone()),
            axum::extract::Path(post.id),
            axum::Json(create_post),
        )
        .await
        .expect("Failed to update post.");

        assert_eq!(response, http::StatusCode::NO_CONTENT);
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_update_post_not_found(pool: PgPool) {
        let create_post = post::CreatePost {
            title: String::from("New Title"),
            body: String::from("New Body"),
            slug: String::from("slug"),
        };

        let response = posts::update_post(
            axum::extract::State(pool.clone()),
            axum::extract::Path(uuid::Uuid::new_v4()),
            axum::Json(create_post),
        )
        .await;

        match response {
            Ok(_) => assert!(false),
            Err(err) => assert_eq!(err, http::StatusCode::NOT_FOUND),
        }
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_delete_post(pool: PgPool) {
        let get_posts_res = posts::get_posts(axum::extract::State(pool.clone()))
            .await
            .expect("Failed to get posts.")
            .0;

        let post = get_posts_res.first().unwrap();

        let response = posts::delete_post(
            axum::extract::State(pool.clone()),
            axum::extract::Path(post.id),
        )
        .await
        .expect("Failed to delete post.");

        assert_eq!(response, http::StatusCode::NO_CONTENT);
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_delete_post_not_found(pool: PgPool) {
        let response = posts::delete_post(
            axum::extract::State(pool.clone()),
            axum::extract::Path(uuid::Uuid::new_v4()),
        )
        .await;

        match response {
            Ok(_) => assert!(false),
            Err(err) => assert_eq!(err, http::StatusCode::NOT_FOUND),
        }
    }
}
