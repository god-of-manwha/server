use sqlx::{postgres::PgPoolOptions, PgPool};
use std::env;

fn build_database_url() -> String {
    let user = env::var("POSTGRES_USER").expect("missing POSTGRES_USER env");
    let password = env::var("POSTGRES_PASSWORD").expect("missing POSTGRES_PASSWORD env");
    let db = env::var("POSTGRES_DB").expect("missing POSTGRES_DB env");
    let host = env::var("POSTGRES_HOST").expect("missing POSTGRES_HOST env");
    format!("postgresql://{}:{}@{}/{}", user, password, host, db)
}

pub async fn get_database_pool() -> Result<PgPool, sqlx::Error> {
    PgPoolOptions::new()
        .max_connections(5)
        .connect(&build_database_url())
        .await
}
