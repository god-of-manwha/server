use serde::{Deserialize, Serialize};
use sqlx::prelude::FromRow;

#[derive(Debug, Deserialize, Clone)]
pub struct CreatePost {
    pub title: String,
    pub body: String,
    pub slug: String,
}

#[derive(PartialEq, Debug, Serialize, FromRow)]
pub struct Post {
    pub id: uuid::Uuid,
    pub title: String,
    pub body: String,
    pub slug: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

impl Post {
    pub fn new(title: String, body: String, slug: String) -> Self {
        let now = chrono::Utc::now();

        Self {
            id: uuid::Uuid::new_v4(),
            title: title,
            body: body,
            slug: slug,
            created_at: now,
            updated_at: now,
        }
    }
}
