use super::database::get_database_pool;
use crate::controllers::health;
use crate::controllers::posts;

use axum::routing::{delete, get, post, put, Router};

pub async fn routes() -> Result<Router, sqlx::Error> {
    let pool = get_database_pool().await?;

    let router = Router::new()
        .route("/health", get(health::get_health))
        .route("/posts", post(posts::create_post))
        .route("/posts", get(posts::get_posts))
        .route("/posts/:id", get(posts::get_post))
        .route("/posts/:id", put(posts::update_post))
        .route("/posts/:id", delete(posts::delete_post))
        .with_state(pool);

    Ok(router)
}
