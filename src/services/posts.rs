use crate::models::post::{self, CreatePost};
use sqlx::PgPool;

#[derive(Debug, PartialEq)]
pub enum PostError {
    CreateError,
    DuplicateError,
    NotFoundError,
    GetPostsError,
    UpdateError,
    DeleteError,
}

pub async fn create_post(pool: &PgPool, post: &post::Post) -> Result<(), PostError> {
    let res = sqlx::query(
        r#"
        INSERT INTO posts(id, title, body, slug, created_at, updated_at)
        VALUES ($1, $2, $3, $4, $5, $6)
        "#,
    )
    .bind(post.id)
    .bind(post.title.clone())
    .bind(post.body.clone())
    .bind(post.slug.clone())
    .bind(post.created_at)
    .bind(post.updated_at)
    .execute(pool)
    .await;

    match res {
        Ok(_) => Ok(()),
        Err(err) => {
            let Some(database_error) = err.as_database_error() else {
                return Err(PostError::CreateError);
            };

            let Some(code) = database_error.code() else {
                return Err(PostError::CreateError);
            };

            match code.as_ref() {
                "23505" => Err(PostError::DuplicateError),
                _ => Err(PostError::CreateError),
            }
        }
    }
}

pub async fn get_post_by_uuid(pool: &PgPool, uuid: &uuid::Uuid) -> Result<post::Post, PostError> {
    let res = sqlx::query_as::<_, post::Post>("SELECT * FROM posts WHERE id = $1")
        .bind(uuid)
        .fetch_one(pool)
        .await;

    match res {
        Ok(post) => Ok(post),
        Err(_) => Err(PostError::NotFoundError),
    }
}

pub async fn get_post_by_slug(pool: &PgPool, slug: &String) -> Result<post::Post, PostError> {
    let res = sqlx::query_as::<_, post::Post>("SELECT * FROM posts WHERE LOWER(slug) = LOWER($1)")
        .bind(slug)
        .fetch_one(pool)
        .await;

    match res {
        Ok(post) => Ok(post),
        Err(_) => Err(PostError::NotFoundError),
    }
}

pub async fn get_posts(pool: &PgPool) -> Result<Vec<post::Post>, PostError> {
    let res = sqlx::query_as::<_, post::Post>("SELECT * FROM posts")
        .fetch_all(pool)
        .await;

    match res {
        Ok(posts) => Ok(posts),
        Err(_) => Err(PostError::GetPostsError),
    }
}

pub async fn update_post(
    pool: &PgPool,
    uuid: &uuid::Uuid,
    post: &CreatePost,
) -> Result<(), PostError> {
    if let Err(_) = get_post_by_uuid(pool, uuid).await {
        return Err(PostError::NotFoundError);
    }

    let updated_at = chrono::Utc::now();
    let res = sqlx::query(
        r#"
        UPDATE posts
        SET title = $1, body = $2, slug = $3, updated_at = $4
        WHERE id = $5
        "#,
    )
    .bind(post.title.clone())
    .bind(post.body.clone())
    .bind(post.slug.clone())
    .bind(updated_at)
    .bind(uuid)
    .execute(pool)
    .await;

    match res {
        Ok(_) => Ok(()),
        Err(_) => Err(PostError::UpdateError),
    }
}

pub async fn delete_post(pool: &PgPool, uuid: &uuid::Uuid) -> Result<(), PostError> {
    if let Err(_) = get_post_by_uuid(pool, uuid).await {
        return Err(PostError::NotFoundError);
    }

    let res = sqlx::query(
        r#"
        DELETE FROM posts
        WHERE id = $1
        "#,
    )
    .bind(uuid)
    .execute(pool)
    .await;

    match res {
        Ok(_) => Ok(()),
        Err(_) => Err(PostError::DeleteError),
    }
}
