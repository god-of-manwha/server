#[cfg(test)]
mod tests {
    use crate::models::post;
    use crate::services::posts::{self, PostError};
    use sqlx::PgPool;

    #[sqlx::test(fixtures("posts"))]
    async fn test_get_posts(pool: PgPool) {
        let response = posts::get_posts(&pool).await.expect("Failed to get posts.");

        assert_eq!(response.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_post_uuid_not_found(pool: PgPool) {
        let response = posts::get_post_by_uuid(&pool, &uuid::Uuid::new_v4()).await;

        let Err(error) = response else {
            return assert!(false);
        };

        assert_eq!(error, PostError::NotFoundError)
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_get_post_uuid(pool: PgPool) {
        let posts = posts::get_posts(&pool).await.expect("Failed to get posts.");

        for post in posts {
            let response = posts::get_post_by_uuid(&pool, &post.id)
                .await
                .expect("Failed to get post.");

            assert_eq!(response, post);
        }
    }

    #[sqlx::test]
    async fn test_get_post_slug_not_found(pool: PgPool) {
        let response = posts::get_post_by_slug(&pool, &String::from("random-slug")).await;

        let Err(error) = response else {
            return assert!(false);
        };

        assert_eq!(error, PostError::NotFoundError)
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_get_post_slug(pool: PgPool) {
        let posts = posts::get_posts(&pool).await.expect("Failed to get posts.");

        for post in posts {
            let response = posts::get_post_by_slug(&pool, &post.slug)
                .await
                .expect("Failed to get post.");

            assert_eq!(response, post);
        }
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_create_duplicate_post(pool: PgPool) {
        let post = post::Post::new(
            String::from("Some Title"),
            String::from("Some Body"),
            String::from("random-slug-1"),
        );

        let result = posts::create_post(&pool, &post).await;

        let Err(error_code) = result else {
            return assert!(false);
        };

        assert_eq!(error_code, PostError::DuplicateError);
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_create_post(pool: PgPool) {
        let get_posts_res = posts::get_posts(&pool).await.expect("Failed to get posts.");

        assert_eq!(get_posts_res.len(), 3);

        let post = post::Post::new(
            String::from("Some Title"),
            String::from("Some Body"),
            String::from("some-slug"),
        );

        posts::create_post(&pool, &post)
            .await
            .expect("Failed to create post.");

        let get_posts_res = posts::get_posts(&pool).await.expect("Failed to get posts.");

        assert_eq!(get_posts_res.len(), 4);

        let response = posts::get_post_by_uuid(&pool, &post.id)
            .await
            .expect("Failed to get post.");

        assert_eq!(&response.id, &post.id);
        assert_eq!(&response.title, &post.title);
        assert_eq!(&response.body, &post.body);
        assert_eq!(&response.slug, &post.slug);
    }

    #[sqlx::test]
    async fn test_update_post_not_found(pool: PgPool) {
        let updated_post = post::CreatePost {
            title: String::from("title"),
            body: String::from("body"),
            slug: String::from("slug"),
        };

        let response = posts::update_post(&pool, &uuid::Uuid::new_v4(), &updated_post).await;

        let Err(error) = response else {
            return assert!(false);
        };

        assert_eq!(error, PostError::NotFoundError)
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_update_post(pool: PgPool) {
        let get_posts_res = posts::get_posts(&pool).await.expect("Failed to get posts.");

        let post = get_posts_res.first().unwrap();
        let updated_title = String::from("Updated Title");
        let updated_post = post::CreatePost {
            title: updated_title.clone(),
            body: post.body.clone(),
            slug: post.slug.clone(),
        };

        posts::update_post(&pool, &post.id, &updated_post)
            .await
            .expect("Failed to update post.");

        let get_post_res = posts::get_post_by_uuid(&pool, &post.id)
            .await
            .expect("Failed to get post.");

        assert!(post.updated_at < get_post_res.updated_at);
        assert_ne!(get_post_res.title, post.title);
        assert_eq!(get_post_res.title, updated_title);
        assert_eq!(get_post_res.body, post.body);
        assert_eq!(get_post_res.slug, post.slug);
    }

    #[sqlx::test]
    async fn test_delete_post_not_found(pool: PgPool) {
        let response = posts::delete_post(&pool, &uuid::Uuid::new_v4()).await;

        let Err(error) = response else {
            return assert!(false);
        };

        assert_eq!(error, PostError::NotFoundError)
    }

    #[sqlx::test(fixtures("posts"))]
    async fn test_delete_post(pool: PgPool) {
        let get_posts_res = posts::get_posts(&pool).await.expect("Failed to get posts.");

        assert_eq!(get_posts_res.len(), 3);

        let post = get_posts_res.first().unwrap();

        posts::delete_post(&pool, &post.id)
            .await
            .expect("Failed to delete post.");

        let get_posts_res = posts::get_posts(&pool).await.expect("Failed to get posts.");

        assert_eq!(get_posts_res.len(), 2);
    }
}
